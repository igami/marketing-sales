
# Marketing & Sales

- [Marketing & Sales](#marketing--sales)
- [1. Termin 26. März (09:00-12:30 Uhr)](#1-termin-26-märz-0900-1230-uhr)
  - [Struktur](#struktur)
  - [Prüfung](#prüfung)
  - [Warum ist Goldbeck so gut?](#warum-ist-goldbeck-so-gut)
  - [Beispiel Vapiano](#beispiel-vapiano)
  - [Definition des Marketing Konzept](#definition-des-marketing-konzept)
  - [Institutionelle Besonderheiten des Marketings](#institutionelle-besonderheiten-des-marketings)
  - [Grundlegende Besonderheiten des B2B Marketing](#grundlegende-besonderheiten-des-b2b-marketing)
  - [Klassifizierung von Geschäftstypen im B2B Bereich](#klassifizierung-von-geschäftstypen-im-b2b-bereich)
  - [Markt und relevanter Markt](#markt-und-relevanter-markt)
  - [Problem Abgrenzung des relevanten Marktes](#problem-abgrenzung-des-relevanten-marktes)
  - [Beschreibung des relevanten Marktes anhand von quantitativen Daten](#beschreibung-des-relevanten-marktes-anhand-von-quantitativen-daten)
  - [Konzeptionsebenen des Marketings](#konzeptionsebenen-des-marketings)
  - [Marketing-strategisches Grundraser](#marketing-strategisches-grundraser)
  - [Marktsegmentierung](#marktsegmentierung)
  - [Bildung und Eigenschaften strategischer Geschäftsfelder](#bildung-und-eigenschaften-strategischer-geschäftsfelder)
- [2. Termin 02.04.2022 (09:00 - 15:30 Uhr)](#2-termin-02042022-0900---1530-uhr)
  - [Beispiel CLAAS](#beispiel-claas)
  - [Marktforschung & Kaufverhalten](#marktforschung--kaufverhalten)
  - [Träger der Marktforschung](#träger-der-marktforschung)
  - [Nielsen Gebiete](#nielsen-gebiete)
  - [Eigenmarktforschung](#eigenmarktforschung)
  - [Sekundär-Marktforschung: Fallbeispiel Schokoladenmarkt](#sekundär-marktforschung-fallbeispiel-schokoladenmarkt)
  - [Der Marktforschungsprozess S. 13](#der-marktforschungsprozess-s-13)
  - [Methoden der Erhebung](#methoden-der-erhebung)
  - [Tracking-Forschung: Panel-Forschung](#tracking-forschung-panel-forschung)
  - [Exkurs: Erklärungsansätze des Käuferverhaltens](#exkurs-erklärungsansätze-des-käuferverhaltens)
  - [Aktivierende Prozesse - Zusammenhang zwischen Aktivierung, Emotion, Motivation und Einstellung](#aktivierende-prozesse---zusammenhang-zwischen-aktivierung-emotion-motivation-und-einstellung)
    - [Aktivierung](#aktivierung)
    - [Lambdakurve](#lambdakurve)
    - [Definition Involvement](#definition-involvement)
    - [Involvement-Konstrukt: High Involvement](#involvement-konstrukt-high-involvement)
    - [Involvement-Konstrukt: Low Involvement](#involvement-konstrukt-low-involvement)
    - [Emotionen](#emotionen)
    - [Einstellungen](#einstellungen)
  - [Kundenzufriedenheit](#kundenzufriedenheit)
  - [Werte und Lebensstile](#werte-und-lebensstile)
  - [Lerntheoretische Ansätze](#lerntheoretische-ansätze)
  - [Kaufverhalten von Organisationen](#kaufverhalten-von-organisationen)
  - [Berühmte Marken!](#berühmte-marken)
  - [Wirkungsbezogene Definition des Markenbegriffs](#wirkungsbezogene-definition-des-markenbegriffs)
  - [Funktionen der Marke aus Nachfragersucht](#funktionen-der-marke-aus-nachfragersucht)
  - [Funktionen der Marke aus Anbietersicht](#funktionen-der-marke-aus-anbietersicht)
  - [Vor- und Nachteile der Einzelmarkenstrategie](#vor--und-nachteile-der-einzelmarkenstrategie)
  - [Vor- und Nachteile der Familienmarkenstrategie](#vor--und-nachteile-der-familienmarkenstrategie)
  - [Vor- und Nachteile der Dachmarkenstrategie](#vor--und-nachteile-der-dachmarkenstrategie)
  - [Markenstrategien](#markenstrategien)
  - [Exkurs: Preis- und Qualitätsniveau der Markentypen um vertikalen Wettbewerb](#exkurs-preis--und-qualitätsniveau-der-markentypen-um-vertikalen-wettbewerb)
  - [Der Weg zum Kunden](#der-weg-zum-kunden)
  - [Positionierung](#positionierung)
  - [Prozess der Markengestaltung](#prozess-der-markengestaltung)
  - [Modifiziertes Markensteuerrad nach Esch](#modifiziertes-markensteuerrad-nach-esch)
  - [Markenaufbau durch Markierung und durch Kommunikation](#markenaufbau-durch-markierung-und-durch-kommunikation)
  - [Markennamen-Matrix](#markennamen-matrix)

# 1. Termin 26. März (09:00-12:30 Uhr)

## Struktur

- selbst lernen anhand der Studienbriefe
- Präsenz ist drumrum und zum diskutieren
- Marketing ist wichtig im B2B und kommt aus dem Konsumgüterbereich
- wir fangen von vorne an
-Marketing ist eine verbale Sache und weniger rechnen, trotzdem konzeptionell
-Thema heute: Studienbrief Nr. 1

## Prüfung

- Prüfung ist total spannend und mündlich als 
- 4 Wochen vorher bekommen wir die Möglichkeit uns ein Thema auszusuchen und über das muss 15 Minuten, mit einem Foliensatz, in der 30 minütigen Prüfung referiert werden. Danach kommen noch 4-5 Fragen.
- nach Erfahrung die beste art der Prüfung, da die studierenden sehr motiviert daran teilnehmen und wesentlich mehr Kompetenzen abgefragt werden
- Frau Kruschel ist die Beisitzerin

## Warum ist Goldbeck so gut?

Goldbeck bietet das Rundum sorglos Paket von Planung, Bau und Betrieb

Probleme beim Bauen
- muss mit anderen Gewerken zusammen spielen

Goldbeck will etwas ganz besonderes anbieten, daher wurde bei einem Marken Relaunch das "Bau" gestrichen.
Netzwerk innovatives bauen - Dienstleistungsorientiert => hat sich den ganzen Prozess angeschaut um Kundenbindung zu erreichen und den Kunden nach dem bauen noch länger zu betreuen

Runter brechen in verschiedene Marken hat nicht funktioniert
da der Aufwand für die Werbemaßnahmen zu groß ist

Kundenbetreuung direkt vor Ort und Zielgruppenorientiert
Marke wirkt nicht nur nach außen, sondern wird auch durch die Mitarbeiter getragen.

## Beispiel Vapiano

ist mittlerweile Pleite, hat aber noch 33 Stammgeschäfte
Insolvenz weil die Qualität nicht gestimmt hat und die Mitarbeiten schlecht behandelt wurden

Was fällt Ihnen zu Vapiano ein:
- Selfservice
- Erlebnis direkt in der Küche zu stehen

Vapiano hat in einem gesättigten Markt mit einer guten Idee platziert

Wie kann man eine Positionierung im Markt vornehmen?
- Koordinatensystem Preis / Qualität (Leistung)
- Wer bietet im Markt Qualität in Bereich Preis / Qualität an? Von McDonalds bis edel Italiener (Marktanalyse)
- Lücke finden und dort platzieren

## Definition des Marketing Konzept

Marketing bezeichnet die Planung, Koordination und Kontrolle aller auf aktuelle und potenzielle Märkte ausgerichtete Unternehmensaktivitäten zur Verwirklichung der Unternehmensziele durch eine dauerhafte Befriedigung der Kundenbedürfnisse.

Es gibt zwei Sichtweisen des Marketing
- Marketing als Unternehmensfunktion
- Marketing als Führungskonzept

Kennen verschiedener Strategien im Marketing zur Differenzierung.
Heute ist Kundenbindung der entscheidende Faktor.

## Institutionelle Besonderheiten des Marketings

- Dienstleistungsmarketing
- Handelsmarketing
- Industriegütermarketing

Entscheidung im Industriegütermarketing ist nicht nur von einer Person (GeschäftsführerInnen) sonder vom Buying-Center (GeschäftsführerInnen, EinkäuferInnen, BedienerInnen) abhängig.
B2B Marketing inkludiert Industriegütermarketing und Dienstleistungsmarketing.

Meffert ist der "Papst des Marketing" in Deutschland
Homberg, Backhaus und Klein-Altenkamp werden von Rössler empfohlen

## Grundlegende Besonderheiten des B2B Marketing

- Abgeleiteter Charakter der Nachfrage
- Multipersonalität
- Multiorganisationalität
- Hoher Formalisierungsgrad
- Hoher Individualisierungsgrad
- Besondere Bedeutung der Geschäftsbeziehung
- Hoher Grad der Interaktion

## Klassifizierung von Geschäftstypen im B2B Bereich

- Systemgeschäft
- Zuliefergeschäft
- Produktgeschäft/Spotgeschäft
- Anlagengeschäft/Projektgeschäft

## Markt und relevanter Markt

Markt aus betriebswirtschaftlicher Sicht, nicht aus volkswirtschaftlicher Sicht

## Problem Abgrenzung des relevanten Marktes

Kriterien der Marktabgrenzung
- Sachlich: Welche Arten von Leistungen werden am Markt angeboten?
- Zeitlich: Ist der Markt zeitlich begrenzt?
- Räumlich: Ist der Markt lokal, regional, national oder international?

## Beschreibung des relevanten Marktes anhand von quantitativen Daten

- Marktvolumen: gegenwärtige von allen Anbietern abgesetzte Menge für eine Produktgattung.
- Marktpotenzial: Gesamtheit aller möglichen Absatzmengen eines Marktes für eine bestimmte Produktgattung
- Marktausschöpfungsgrad/Marktsättigung: bezieht das Marktvolumen auf das Marktpotenzial
- Absatzvolumen: Absatzmenge des Produktes eines Unternehmens
- Marktanteil in %: Absatzmenge (pro Zeiteinheit) bezogen auf das Marktvolumen (pro Zeiteinheit) x 100
- Relativer Marktanteil: Marktanteil des eigenen Unternehmens bezogen auf den Marktanteil des stärksten Konkurrenten

## Konzeptionsebenen des Marketings

1. Ebene **Marketingziele** (= Bestimmmung der "Wunschorte") => Wo wollen wir hin?
2. Ebene **Marketingstrategien** (= Festlegen der "Route") => Wie kommen wir dahin?
3. Ebene **Marketingmix** (= Wahl der "Beförderungsmittel") => Was müssen wir dafür einsetzen?

Literatur: Becker 

## Marketing-strategisches Grundraser

| Vier Strategieebenen             | Art der strategischen Festlegung                                     | Strategische Basisoptionen                                          |
| -------------------------------- | -------------------------------------------------------------------- | ------------------------------------------------------------------- |
| 1. Marktfeldstategien            | Festlegung der Art der Produkte/Markt-Kombination(en)                | Gegenwärtige oder neue Produkte in gegenwärtigen oder neuen Märkten |
| 2. Makrtstimulierungsstrategien  | Bestimmung der Art und Weise der Marktbeeinflussung                  | Qualitäts- oder Preiswettbewerb                                     |
| 3. Marktparzellierungsstrategien | Festlegung von Art und Grad der Differenzierung der Marktbearbeitung | Massenmarkt- oder Segmentierungsmarketing                           |
| 4. Marktarealstrategien          | Bestimmung der Art und Stufe des Markt- bzw. Absatzraumes            | Nationale oder internationale Absatzpolitik                         |

Literatur: Becker

Ansoff Matrix
- Marktdurchdringung
- Marktentwicklung
- Produktentwicklung
- Diversifikation

Unterschied kennen zwischen horizontaler/vertikaler/lateraler Diversifizierung (Skript Seite 47f)

## Marktsegmentierung

- quantitative Merkmale (Alter, Geschlecht, Einkommen, ...)
- qualitative Merkmale (Persönlichkeitsmerkmale, Bedürfnisse, Interessen, ...)

Dreistufige Segemntierungsansätze nach Scheuch oder Gröne

## Bildung und Eigenschaften strategischer Geschäftsfelder

# 2. Termin 02.04.2022 (09:00 - 15:30 Uhr)

## Beispiel CLAAS

Unternehmenshistorie aus Marketingsicht

| Meilenstein                                | Marktstrategie                                   |
| ------------------------------------------ | ------------------------------------------------ |
| Knoter                                     | keine Strategie                                  |
| Mäh-Drescher-Binder                        | Diversifikation (neues Produkt, gleicher Markt)  |
| Mähdrescherspezialist                      | Marktstimulierung (Qualität & Hochpreis)         |
| Erntespezialist                            | Diversifikation (neues Produkt, neuer Markt)     |
| Internationales Wachstum & Digitalisierung | Diversifikation (neue Produkte in neuen Märkten) |

## Marktforschung & Kaufverhalten

Aufgabe der Marktforschung ist die systematische und objektive Sammlung, Analyse und Interpretation von Daten über Märkte, Wettbewerber und Zielgruppen, um Marketing-Entscheidungen zu unterstützen.

- Marktforschung
  - Marktinformationen
    - Beschaffungsmarktforschung
    - Absatzmarktforschung
- Marketingforschung
  - Umweltinformationen
  - Inerneinformationen

## Träger der Marktforschung

- Eigenmarktforschung
  - zentrale Stabsabteilung
  - dezentrale Marktforschungsabteilung
  - einzelne Marktforscher
- Fremdmarktforschung
  - Vollserviceinstitute
  - spezialisierte Institute
  - Einzelberater
  - Kommunikationsagenturen
  - Verlage
  - sonstige Informationsquellen
  - (social Media / Meta / Facebook / Instagram / ...)
- Kombination von Eigenmarkt- und Fremdmarktforschung

## Nielsen Gebiete

> Als Nielsengebiete (auch Nielsen-Gebiete) bezeichnet man eine von dem Marktforschungsunternehmen The Nielsen Company ursprünglich für die regionale Erhebung, Auswertung und Aufbereitung der eigenen Marktstudien eingeführte Aufteilung Deutschlands und Österreichs in verschiedene Regionen.
> https://de.wikipedia.org/wiki/Nielsengebiet


## Eigenmarktforschung

- Welche Aufgaben fallen nach Art und Umfang zu welchen zeitlichen Intervallen an?
- Wie ist die personelle und sachliche Ausstattung?
- Ist betriebliche Marktforschung günstiger als Fremdbezug?
- **Organisation**
  - meist Stabsstelle des Marketing oder der Geschäftsführung
  - Interne Organisation funktional oder divisional
  - CLAAS
    - Qualitative Marktforschung
    - Quantitative Marktforschung
    - Corporate Pricing

## Sekundär-Marktforschung: Fallbeispiel Schokoladenmarkt

Quellen Prüfen!

## Der Marktforschungsprozess S. 13

- Informationsbedarf
- Erhebung
- Analyse
- Interpretation/Präsentation
- Entscheidung


## Methoden der Erhebung

- Beobachtung
- Befragung
- Experiment


## Tracking-Forschung: Panel-Forschung

- in Langzeitstudien werden Erhebungen  in zeitlichen Abständen mit gleichem Untersuchungsdesign durchgeführt
- wachsende Bedeutung von Panels:
  - schnelle Veränderungen des Kaufverhaltens
  - tragen kontinuierlich zum Umsatz des Marktforschungsunternehmens bei
- Verbraucherpanel
- Handelspanel
  - Handelspanels sind regelmäßige Erhebungen bei Ausgewählten Absatzmittlern
    - Markenartikler können Distribution beobachten
    - Handel kann seine Leistung erkennen
    - A.C. Nielsen begündet 1958 Handelspanels in Deutschland


## Exkurs: Erklärungsansätze des Käuferverhaltens

- Es gibt unterschiedliche Motive die das Kaufverhalten beeinflussen
  - Preis
  - Marke
  - ...
- Forschungsansätze des Käuferverhaltens
  - S-R-Modelle (Behavioristischeforschungsansätze)
    - wollen nicht erklären warum etwas passiert, sondern nur, dass es etwas passiert
  - S-O-R Modelle (echte Verhaltensmodelle)
    - Neobehavioristische Forschungsansätze
    - Kognitive Forschungsansätze
  - S = Stimulus
  - O = Organismus
  - R = Response


## Aktivierende Prozesse - Zusammenhang zwischen Aktivierung, Emotion, Motivation und Einstellung 

Literatur: Scharf & Co., Marketing, 2015, S. 70

- Aktivierung + Interpretation
  - Emonitionale Reize (Augen) aktivieren den Konsumenten
- Emotion + Zielorientierung
  - Der Anblick freundlicher Menschen löst positive Emotionen aus
- Motivation + Objektorientierung
  - Mann am Steuerrad und am Bug des Schiffes weckt das Bedürfnis nach Freiheit und Abenteuer
- Einstellung
  - Genusssituation und der Slogan "Folge deinem inneren Kompass" erzeugen bzw. verstärken die positive Einstellung gegenüber der Marke

### Aktivierung

- Die Aktivierung eines Konsumenten ist ein Erregungszustand der den Konsumenten zur Handlungen stimuliert. Organismus wird mit Energie versorgt und in einen Zustand der Leistungsfähigkeit versetzt.
- Reize lösen Aktivierung aus, wobei folgende Reizmuster existieren
  - emotionale Reize
    - rufen biologisch vorprogrammierte Reaktionen hervor (z.B. Kindchenschema)
  - kognitive Reize
    - erzeugen gedankliche Konflikte oder Überraschungen, die Aktivierung auslösen
  - physische Reize
    - besondere physische Beschaffenheit von Objekten führt zu Aktivierung (große Werbeplakate)
- Aktivierung wirkt sich auf Verhalten aus: mit zunehmender Aktivierung steigt die Bereitschaft zur Informationsverarbeitung einer Werbebotschaft

### Lambdakurve

### Definition Involvement

- Involvement bezeichnet den Grad der „Ich-Beteiligung“ bzw. des Engagements einer Person, sich für bestimmte Sachverhalte oder Aufgaben zu interessieren und einzusetzen. Es ist die auf den Informationserwerb und die Informationsverarbeitung gerichtete Aktivität des Nachfragers und damit ein spezielles Sub-Konstrukt der Aktiviertheit.
- Konstrukt: High-Involvement und Low Involvement

### Involvement-Konstrukt: High Involvement

High-Involvement-Käufe:
- Käufe sind wichtig für den Konsumenten und haben eine enge Verbindung zu seiner individuellen Persönlichkeit und Selbsteinschätzung;
- Konsument nimmt gewisses finanzielles, soziales, psychologisches oder gesundheitliches Risiko in Kauf;
- viel Zeit- und Energieaufwand bei komplexen Entscheidungsprozessen für Auswahl von Produktalternativen;
- Beispiele: Hauskauf, Erwerb von Luxusmarken oder Entscheidung für medizinischen Eingriff im Krankenhaus


### Involvement-Konstrukt: Low Involvement

Low-Involvement-Käufe:
- begrenzte Entscheidungsprozesse
- weniger wichtig für Konsumenten
- nur mit geringen Risiken verbunden
- durch verfestigte Verhaltensmuster bestimmt
- Beispiele: Kauf von generischen Produkten wie Zucker, Salz oder Toilettenpapier

### Emotionen

- Emotionen sind augenblickliche oder anhaltenden Gefühlszustände
- Hierzu zählen zehn angeborene primäre emotionale Grundhaltungen: Interesse, Freude, Überraschung, Kummer, Zorn, Ehre, Geringschätzung, Furcht, Scham und Schuld
- Unternehmen wollen Produkte durch emotionale Erlebnisse unterscheidbar machen (emotionale Konditionierung = Freude am Fahren)
- Konsumenten können sich besser an Produkte erinnern, wenn bei ihnen Emotionen erzeugt wurden


### Einstellungen

Einstellungen sind innere Denkhaltungen des Konsumenten gegenüber einer Person, Idee oder Sache, verbunden mit einer Wertung oder Erfahrung. Diese Denkhaltung wird erlernt und ist relativ zeitbeständig.

Einstellungen entstehen durch Lernprozesse, bei denen der Konsument selbst Erfahrungen mit dem Bezugsobjekt macht. Er kann auch durch Erfahrungen anderer lernen.

https://de.wikipedia.org/wiki/4711


## Kundenzufriedenheit

Ergebnis eines mehr oder weniger komplexen Vergleichsprozesses zwischen den Erwartungen des Kunden an ein Produkt oder eine Dienst- leistung (Soll-Leistung) und der tatsächlich erlebten Befriedigung durch das Produkt oder die Dienstleistung (Ist-Leistung).

Im Gegensatz zur Einstellung geht der Entstehung von Zufriedenheit bzw. Unzufriedenheit mit einem Produkt/einer Dienstleistung die unmittelbare Erfahrung mit diesem Produkt/dieser Dienstleistung voraus.

siehe Kano-Modell der Kundenzufriedenheit

https://de.wikipedia.org/wiki/Kano-Modell

https://www.marconomy.de/emotion-ist-alles-in-der-kundenbindung-a-405021/

https://de.wikipedia.org/wiki/Semantisches_Differenzial

https://www.karstennoack.de/wie-wirken-sie-wirklich/


## Werte und Lebensstile

## Lerntheoretische Ansätze


## Kaufverhalten von Organisationen

Mitglieder eines Buying Centers und ihre Rollen
- Entscheider (Entscheided, Budget)
- Gegner (Fördert Wettbewerb)
- Coach (Türöffner, Ratgeber)
- Anwender (definiert Bedarf)
- Wächter (selektiert Lösungen und Lieferanten)

## Berühmte Marken!

Mercedes
- alte weiße Männer
- Sicherheit
- wäre anders, wenn es statt einer C-Klasse eine A-Klasse oder ein SLK wäre

Coca Cola
- Weihnachten

## Wirkungsbezogene Definition des Markenbegriffs

Alle Produkte sind im weiteren Sinne dann Marken, wenn sie ein klares, unverwechselbares Image bei den Konsumenten aufgebaut haben bzw. aufbauen werden.

"Marken sind Vorstellungsbilder in den Köpfen der Anspruchsgruppen, die eine Identifikation- und Differenzierungsfunktion übernehmen und das Wahlverhalten prägen."

Vorstellungsbilder = alle Assoziationen die ein Konsument mit einer Marke verbindet

=> Durch Marken können die Wahrnehmungen und Präferenzen der Konsumenten und folglich auch deren Kaufverhalten nachhaltig beeinflusst werden.

## Funktionen der Marke aus Nachfragersucht

- Orientierungshilfe (Identifizierungsfunktion)
- Entlasungfunktion
- Qualitätssicherungsfunktion
- Identifikationsfunktion
- Prestigefunktion
- Vertrauensfunktion

## Funktionen der Marke aus Anbietersicht

- Präferenzbildung
- Wertsteigerung des Unternehmens
- Preispolitischer Spielraum
- Plattform für neue Produkte
- Segmentspezifiesce Marktbearbeitung
- Kundenbindung
- Differenzierung gegenüber der Konkurrenz

## Vor- und Nachteile der Einzelmarkenstrategie

Vorteile:
- Möglichkeit der klaren Profilierung eines Produkts
- Konzentration auf eine deinierte Zielgruppe
- Wahl einer spezifischen Positionierung gegeben
- Innivationscharakter des neuen Produkts lässt sich gut darstellen
- Relaunch-Maßnahmen sind gut umzusetzen
- Bei Misserfolg des Produkts erfolgt kein negativer Imagetransfer auf andere Produkte des Unternehmens

Nachteile:
- Ein Produkt muss dsa gesamte Markenbudget allein tragen
- Voraussetzung ist ein ausreichendes Marktvolumen bzw. -potenzial
- Mrkenpersönlichkit wird nun langsam aufgebaut
- Immer kürzere Produktlebenszyklen gefährden die Erreichung des Break Even Point
- Durch Strukturwandel von Märkten kann die Überlebensfähigkeit produktspezifischer Marken gefährdet sein
- geeignete und schutzfähige Marken sind schwer zu finden

## Vor- und Nachteile der Familienmarkenstrategie

Vorteile
- spezifische Prodilierungsmöglichkeit
- mehrere Produkte tragen das Markenbudget
- neue Produkte profitieren vom positiven Imagetransfer der Marke
- Stärkung des Markenimages, wenn das Produkt notwendigen "fit" hat

Nachteile:
- Relaunch-Maßnahmen begrenzt
- "Markenkern" der Ausgangsmarkte begrenzte Innovationsfähigkeiten
- Nur dort einsetzbar, wo Abnehmemer Angebotssysteme mit Nutzenklammern akzeptieren
- Gefahr der Markenüberdehnung bzw. -verwässerung

## Vor- und Nachteile der Dachmarkenstrategie

Vorteile:
- alle Produkte tragen das Markenbudget
- leichte Einführung neuer Produkte
- neue Produkte profitieren vom positiven Imagetransfer des Unternehmens
- Präsenz in kleinen Teilmärkten möglich
- Suche nach eneuen schutzfähigen Marken entfällt

Nachteile:
- klare Profilierung wird erschwert
- Positionierung eher unspezifisch
- Konzentration auf einzelne Zielgruppen ist kaum möglich
- Innovationen können nicht spezifisch profiliert werden
- bei Misserfolg negativer Imagetransfer auf die Marke und alle Produkte

## Markenstrategien

- Dachmarken
- Familienmarken
- Verarbeitungsmarken
- begleitende Marken
- Einzelmarken

## Exkurs: Preis- und Qualitätsniveau der Markentypen um vertikalen Wettbewerb

## Der Weg zum Kunden

## Positionierung

- Ein Produktnutzen
  - Colgate = Schutz gegen Karies
  - Mercedes = Qualität
- USP (unique selling proposition = unverwechselbares Nutzenangebot)
  - beste Qualität, beste Dienstleistung, bester Preis
  - Käufer können sich Nr. 1 gut Merken
- Doppelnutzen
  - Volvo = am sichersten, am langlebigsten
  - falls zwei Wettbewerber beanspruchen, bei einer Eigenschaft die Besten zu sein
- Dreifachnutzen
  - Odol-med 3 = Schutz vor Karies, Paradontose, Zahnstein

## Prozess der Markengestaltung

!! zuhause ansehen !!

## Modifiziertes Markensteuerrad nach Esch

!! zuhause ansehen !!

## Markenaufbau durch Markierung und durch Kommunikation

!! zuhause ansehen !!

## Markennamen-Matrix

!! zuhause ansehen !!
